# Introduction to Ramda

* Functions as first class citizen

## Pure funcions

* given the same input always returns the same output
* never mutate parameters
* always return new state

## Currying
 - A function that can be partially applied
 - ie Function that returns a function
 - Used to make more specific functions
 - see ramda's `curry` function -- covers at end

```
// Partially applied prop function
const prop = key => obj => obj[key];

const ageProp = prop('age');

const goat = {
    name: 'Billy',
    age: 10
};

const age = ageProp(goat); // function only runs when all parameters are supplied
```

## Point-free

 - Separation of logic and data
 - Put the data last
 - Curry all the things
 - Parameters are evil
 - Less to name :)

### Not point-free:

```
const ageProp = goat => prop('age')(goat);
```

### Point-free:

```
const ageProp = prop('age');
```

# Say hello to my little Friend
## Ramda

## Basics

Pretty much all the things in JS and more

## All your typical iterators
## But putting the data last
```
const goats = [
  "Kevin Heritage",
  "Luke Warren",
  "Alex"
];
```

- normie js egs

### Map
* normie JS mode
```
const goatToString = name => (`Goat ${name}`);

const newGoats = goats.map(goatToString);

/*
Result:
  Goat Kevin Hperitage
  Goat Luke Warren
  Goat Alex
*/
```
* Turbo goat mode
```
const goatToString = name => (`Goat ${name}`);

const newGoats = map(goatToString, goats);

/*
Result:
  Goat Kevin Hperitage
  Goat Luke Warren
  Goat Alex
*/
```
### For Each

```
forEach(console.log, goats);

/*
Result: 
  Kevin Heritage
  Luke Warren
  Alex
*/
```



### Reduce -- replace with find

I think we need a better example of this reduce

```
const eqAlex = equals('Alex');

const findAlex = find(eqAlex);

const hereIsAlex = findAlex(goats);

/*
Result:
  Alex
*/
```

### any ---

## List processing - the diagram

```
// extractAgeAndName() --> goats --> sortByAge() --> sortedGoats ---> getTopGoat() --> eldestGoat
```


## Composability

 - The ability to use 2 or more functions together max of 6
 - Should aid readability
 - Ramda makes life easy
 
 
Normie JS mode
```
const eldestGoat = getTopGoat(sortByAge(extractAgeAndName(goats))); // this is not very readable
```

Introducing...

### Compose

Data flows right to left


``` js
const getEldestGoat = compose(
  getTopGoat,
  sortByAge,
  extractAgeAndName
);

const eldestGoat = getEldestGoat(goats);

// eldestGoat <-- getTopGoat() <-- sortedGoats <-- sortByAge() <-- extractAgeAndName() <-- goats
```

### Pipe

Data flows Left to right

``` js
const getEldestGoat = pipe(
  extractAgeAndName,
  sortByAge,
  getTopGoat
);

const eldestGoat = getEldestGoat(goats);

// goats --> extractAgeAndName() --> sortByAge() --> sortedGoats ---> getTopGoat() --> eldestGoat
```

## Debugging

* Debugging in ramda can be tricky because where do you place your break point?
* Sometimes you have to move away from the point free-ness of ramda in order to inspect your functions


### Breaking point free

``` js
// the original code
let newGoats = map(updateGoat, data);


// the debugging code
newGoats = map(goat => {
  console.log('Before: ', goat);
  const newGoat = updateGoat(goat);
  console.log('After:', newGoat);
}, goats);
```

### Without breaking point free
``` js
// a reusable point free example
const wrapper = fn => data => {
  console.log('Before: ', data);
  const output = fn(data);
  console.log('After: ', output);
  return output;
}

newGoats = map(wrapper(updateGoat));
```

### Compose

``` js
const probe = message => data => {
  console.log(message, data);
  return data;
};

const newExtractedGoats = compose(
  probe('Updated goats'),
  updateTheGoats,
  probe('Extraction of goats'),
  extractGoats
)(goats);
```

## Advanced features - place cool name here - but wait theres more

* apply spec
``` js
const goatSpec = applySpec({
  goatName: prop('name'),
  goatAge: prop('age')
});

const goat = goatSpec(rawGoatJson);
```
* curry
```
const findSuperGoat = (goatRoles, goats) => { /*...*/ };

const findSuperGoatVindaloo = curry(findSuperGoat);

const superGoatWithRoles = findSuperGoatVindaloo(rolesOfGoat);

const theSuperGoat = superGoatWithRoles(goats);
```
* composeP/pipeP


## It's a Journey -- don't stop... believing

* start with a sprinkle of Ramda before going full goatard
* its a mindset shift and it takes some time to get used to

## Ramda tutorial
http://randycoulman.com/blog/categories/thinking-in-ramda/


# "What function should I use?" - Matt
https://github.com/ramda/ramda/wiki/What-Function-Should-I-Use%3F


### Resources
https://www.sitepoint.com/currying-in-functional-javascript/

### Resources
http://lucasmreis.github.io/blog/pointfree-javascript/
http://randycoulman.com/blog/2016/06/21/thinking-in-ramda-pointfree-style/
